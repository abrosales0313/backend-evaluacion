const {response} = require('express');
const Auto = require('../model/Auto');
const Mantenimiento = require('../model/Mantenimiento')

const obtenerAutos = async (req, res = response) => {
    try {
        console.log('Entro');
        const autos = await Auto.find();

        res.json(autos)
    } catch (error) {
        console.log(error);
        res.status(400).json({ message: error})
    }
};

const actualizarAuto = async (req, res = response) => {
    
    try {
        const mantenimiento = new Mantenimiento({
            id: req.params.idAuto,
            ...req.body
        });
        await mantenimiento.save();
        res.json({
            ok: true
        })
    } catch (error) {
        console.log(error);
        res.status(400).json({ ok: false, errores: error})
    }

}

module.exports = {
    obtenerAutos,
    actualizarAuto
}