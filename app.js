const express = require('express');
const app = express();
const { dbConnection } = require('./database/config');
var cors = require('cors')
require('dotenv').config();

app.use(cors())
const bodyParser = require('body-parser');

// leemos body
app.use(bodyParser.json());

//Base de datos
dbConnection();

// Importamos rutas
app.use('/autos',require('./routes/autos'))

// Directorio publico
app.use(express.static('public'));

// Escuchando en puerto 
app.listen(process.env.PORT, () => {
    console.log(`Servicor iniciado en el puerto ${process.env.PORT}`);
});