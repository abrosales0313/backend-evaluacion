/*
    Rutas de autos
    host + /autos
*/

const {Router} = require('express');
const router = Router();
const {check}  = require('express-validator')

const { obtenerAutos, actualizarAuto } = require('../controllers/autos');
const { validarCampos } = require('../middlewares/validacion-campos');

// Obtener todos los autos
router.get('/', obtenerAutos);

// actualizar status auto
router.put('/:idAuto/mantenimiento', 
    [// middlewares
    check('idAuto','El id es necesario').not().isEmpty(),    
    check('nombre','El nombre es necesario').not().isEmpty(),
    check('fechaEstimada','la fecha es necesaria').not().isEmpty(),
    validarCampos
    ],
    actualizarAuto    
);

module.exports = router;