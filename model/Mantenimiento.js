const mongoose = require('mongoose');

const MantenimientoSchema = mongoose.Schema({
    id: {
        type: Number,
        require: true
    },
    nombre: {
        type: String,
        require: true
    },
    fechaEstimada: {
        type: String,
        require: true
    }

})

module.exports = mongoose.model('Mantenimiento', MantenimientoSchema);