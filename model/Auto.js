const mongoose = require('mongoose');

const AutosSchema = mongoose.Schema({
    description: String,
    make: String,
    model: String,
    km: Number,
    id: Number,
    image: String

})

module.exports = mongoose.model('Autos', AutosSchema);